<?php
/*
 * Plugin Name: WooCommerce GladePay Payment Gateway
 * Plugin URI: https://developer.gladepay.com
 * Description: Accept payments on your store.
 * Author: Abubakar Siddiq Ango
 * Author URI: http://abuango.me
 * Version: 1.1.2
 *

 /**
 * Check if WooCommerce is active
 **/
if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {


	/**
	* This action hook registers our PHP class as a WooCommerce payment gateway
	*/
	add_filter('woocommerce_payment_gateways', 'gladepay_add_gateway_class');
	function gladepay_add_gateway_class($gateways)
	{
		$gateways[] = 'WC_GladePay_Gateway'; // your class name is here
		return $gateways;
	}


	/**
	* The class itself, please note that it is inside plugins_loaded action hook
	*/
	add_action('plugins_loaded', 'gladeay_init_gateway_class');
	function gladeay_init_gateway_class()
	{
		class WC_GladePay_Gateway extends WC_Payment_Gateway
		{
			/**
			 * Class constructor, more about it in Step 3
			 */
			public function __construct()
			{
				$this->id = 'gladepay'; // payment gateway plugin ID
				$this->icon = 'https://glade.ng/glade.png'; // URL of the icon that will be displayed on checkout page near your gateway name
				$this->has_fields = true;
				$this->method_title = 'GladePay Payments';
				$this->method_description = 'This is a wrapper for GladePay Payment Gateway.'; // will be displayed on the options page

				// gateways can support subscriptions, refunds, saved payment methods,
				// but in this tutorial we begin with simple payments
				$this->supports = array(
					'products'
				);

				// Method with all the options fields
				$this->init_form_fields();

				// Load the settings.
				$this->init_settings();
				$this->title = $this->get_option('title');
				$this->description = $this->get_option('description');
				$this->enabled = $this->get_option('enabled');
				$this->testmode = 'yes' === $this->get_option('testmode');
				$this->customer_fees = $this->get_option('feesbearer');
				$this->merchant_key = $this->testmode ? $this->get_option('test_merchant_key') : $this->get_option('merchant_key');
				$this->merchant_secret = $this->testmode ? $this->get_option('test_merchant_secret') : $this->get_option('merchant_secret');


				// This action hook saves the settings
				add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));

				// We need custom JavaScript to obtain a token
				add_action('wp_enqueue_scripts', array($this, 'payment_scripts'));

			}


			/**
			 * Plugin options, we deal with it in Step 3 too
			 */
			public function init_form_fields()
			{
				$this->form_fields = array(
					'enabled' => array(
						'title' => 'Enable/Disable',
						'label' => 'Enable GladePay Payments',
						'type' => 'checkbox',
						'description' => '',
						'default' => 'no'
					),
					'title' => array(
						'title' => 'Title',
						'type' => 'text',
						'description' => 'This controls the title which the user sees during checkout.',
						'default' => 'GladePay Payments',
						'desc_tip' => true,
					),
					'description' => array(
						'title' => 'Description',
						'type' => 'textarea',
						'description' => 'This controls the description which the user sees during checkout.',
						'default' => 'Pay with diverse payment options using GladePay.',
					),
					'testmode' => array(
						'title' => 'Test mode',
						'label' => 'Enable Test Mode',
						'type' => 'checkbox',
						'description' => 'Place the payment gateway in test mode using test API keys.',
						'default' => 'yes',
						'desc_tip' => true,
					),
					'feesbearer' => array(
						'title' => 'Defer Fees to Customer',
						'label' => 'Make customers pay fees as part of the payment',
						'type' => 'checkbox',
						'description' => 'Enabling this transfers transaction fees to the customer to pay.',
						'default' => 'no',
						'desc_tip' => true,
					),
					'test_merchant_key' => array(
						'title' => 'Test Merchant ID',
						'type' => 'text',
						'default' => 'GP0000001'
					),
					'test_merchant_secret' => array(
						'title' => 'Test Merchant KEY',
						'type' => 'password',
						'default' => '123456'
					),
					'merchant_key' => array(
						'title' => 'Live Merchant ID',
						'type' => 'text'
					),
					'merchant_secret' => array(
						'title' => 'Live Merchant KEY',
						'type' => 'password'
					)
				);
			}

			/**
			 * You will need it if you want your custom credit card form, Step 4 is about it
			 */
			public function payment_fields()
			{
				// ok, let's display some description before the payment form
				if ($this->description) {
					// you can instructions for test mode, I mean test card numbers etc.
					if ($this->testmode) {
						$this->description .= ' <div style="color:#f00; font-weight: bold">TEST MODE ENABLED</div>';
						$this->description = trim($this->description);
					}
					// display the description with <p> tags etc.
					echo wpautop(wp_kses_post($this->description));
				}


				echo '
                    <input id="txnRef" name="txnRef" type="hidden" value="' . $this->order_id . '" />
                    <div id="gladepay-payment-section">
                    </div>
                ';
			}

			/**
            * Custom CSS and JS, in most cases required only when you decided to go with a custom credit card form
            */
			public function payment_scripts()
			{
				// we need JavaScript to process a token only on cart/checkout pages, right?
				if (!is_cart() && !is_checkout() && !isset($_GET['pay_for_order'])) {
					return;
				}

				// if our payment gateway is disabled, we do not have to enqueue JS too
				if ('no' === $this->enabled) {
					return;
				}

				// no reason to enqueue JavaScript if API keys are not set
				if (empty($this->merchant_key) || empty($this->merchant_secret)) {
					return;
				}

				// do not work with card detailes without SSL unless your website is in a test mode
				if (!$this->testmode && !is_ssl()) {
					return;
				}

				// let's suppose it is our payment processor JavaScript that allows to obtain a token
				if ($this->testmode) {
					wp_enqueue_script('gladepay_js', 'https://demo.api.gladepay.com/checkout.js');
				} else {
					wp_enqueue_script('gladepay_js', 'https://api.gladepay.com/checkout.js');
				}

				// and this is our custom JS in your plugin directory that works with token.js
				wp_enqueue_script('woocommerce_gladepay', plugins_url('gladepay.js', __FILE__), array('jquery', 'gladepay_js'));

				// in most payment processors you have to use PUBLIC KEY to obtain a token
				// Added filter to allow 3rd parties to alter the Gladepay parameters.
				// @link https://gitlab.com/gladepay-apis/gladepay-woocommerce-plugin/-/issues/1
				wp_localize_script('woocommerce_gladepay', 'gladepay_params', apply_filters('woocommerce_gladepay_script_params', array(
					'merchant_key' => $this->merchant_key,
					'title' => $this->title,
					'description' => $this->description,
					'total' => WC_Payment_Gateway::get_order_total(),
					'currency' => get_woocommerce_currency() != "USD" ? "NGN" : "USD",
					'order_id' => $this->order_id,
					'customer_fees' => $this->customer_fees == "no" ? "account" : "customer"
				)));

				wp_deregister_script('wc-checkout');
			}


			/**
            * We're processing the payments here, everything about it is in Step 5
            */
			public function process_payment($order_id)
			{
				try {
					global $woocommerce;
					$order = wc_get_order($order_id);

					$txnRef = $_POST['txnRef'];

					if (!$txnRef) {
						wc_add_notice('We could not verify your payment! If you have been charged, please contact our support.', 'error');
						return;
					}

					$args = array(
						'method' => 'PUT',
						'headers' => array(
							'key' => $this->merchant_secret,
							'mid' => $this->merchant_key
						),
						'body' => json_encode(array(
							"action" => "verify",
							"txnRef" => strip_tags($txnRef),
							"customer_txnref" => $order_id,
							"updateOrder" => 1
						))
					);

					if ($this->testmode) {
						$url = 'https://demo.api.gladepay.com/payment';
					} else {
						$url = 'https://api.gladepay.com/payment';
					}

					$response = wp_remote_request($url, $args);

					if (!is_wp_error($response)) {

						$body = json_decode($response['body'], true);

						if ($body['status'] == '200' && strtolower($body['txnStatus']) == 'successful') {
							// we received the payment
							$order->payment_complete();
							//$order->reduce_order_stock();

							// some notes to customer (replace true with false to make it private)
							$order->add_order_note('Hey, your order is paid! Thank you!', true);
							$order->add_order_note('Charged Amount: ' . $body['chargedAmount'], false);
							$order->add_order_note('Transaction Reference Number: ' . $body['txnRef'], false);
							$order->add_order_note('Complete Gateway Response: ' . $response['body'], false);

							// Empty cart
							$woocommerce->cart->empty_cart();

							// Redirect to the thank you page
							return array(
								'result' => 'success',
								'redirect' => $this->get_return_url($order)
							);
						} else {
							wc_add_notice('Error: ' . $body['txnStatus'] . '-' . $body['bank_message'] . ', Please try again.', 'error');
							return;
						}
					} else {
						wc_add_notice('We could not verify your payment, If you have been charged, please contact our support.', 'error');
						return;
					}
				} catch (Exception $e) {
					wc_add_notice($e->getMessage(), 'error');
				}
			}

			/**
            * In case you need a webhook, like PayPal IPN etc
            */
			public function webhook()
			{
			}
		}
	}
} else {
	return "WooCommerce is not active";
}
